﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class PlayerController : NetworkBehaviour
{
    public GameObject bulletPrefab;
    public Transform bulletTransform;
	
	void Update ()
    {
        if (!isLocalPlayer) // Pregunta si es el nuestro
        {
            return;
        }
        var x = Input.GetAxis("Horizontal") * Time.deltaTime * 3.0f;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;
        var rot = Input.GetAxis("Mouse X") * Time.deltaTime * 150.0f;

        transform.Rotate(0, rot, 0);
        transform.Translate(x, 0, z);

        if (Input.GetButtonDown("Fire1"))
        {
            CmdFire();
        }
	}

    [Command]
    void CmdFire()
    {
        var bullet = (GameObject)Instantiate(
            bulletPrefab,
            bulletTransform.position,
            bulletTransform.rotation);

        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;

        NetworkServer.Spawn(bullet);

        Destroy(bullet, 2.0f);
    }

    public override void OnStartLocalPlayer()
    {
        GetComponentInChildren<MeshRenderer>().material.color = Color.blue;
    }
}
