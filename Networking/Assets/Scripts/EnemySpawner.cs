﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class EnemySpawner : NetworkBehaviour{

    public GameObject enemyPrefab;
    public int numberOfEnemies;

    public override void OnStartServer()
    {
        for(int i=0;i<numberOfEnemies;i++)
        {
            var SpawnPosition = new Vector3(Random.Range(-8.0f, 8.0f), 0.0f, Random.Range(-8.0f, 8.0f));

            var SpawnRotation = Quaternion.Euler(0.0f, Random.Range(-0.0f, 180.0f), 0.0f);

            var enemy = (GameObject)Instantiate(enemyPrefab, SpawnPosition, SpawnRotation);
            NetworkServer.Spawn(enemy);
        }

    }
}
