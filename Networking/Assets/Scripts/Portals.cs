﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portals : MonoBehaviour {

    [SerializeField]
    private Transform roomToGo;

    public Transform Teleport()
    {
        return roomToGo;
    }
}
