﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collisions : MonoBehaviour {

    public GameObject player;

    [SerializeField]
    private Health playerHealth;

    private void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.collider.tag == "Enemy")
        {
            playerHealth.TakeDamage(100);
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Portal")
        {
            player.transform.position = other.GetComponent<Portals>().Teleport().position;
        }
    }
}
